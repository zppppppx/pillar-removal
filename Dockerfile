FROM nvcr.io/nvidia/pytorch:22.12-py3


RUN apt-get update
RUN apt update
RUN apt-get install -y libgl1-mesa-glx sudo

# Install dependencies:
RUN ln -snf /usr/share/zoneinfo/$CONTAINER_TIMEZONE /etc/localtime && echo $CONTAINER_TIMEZONE > /etc/timezone
RUN apt-get update && apt-get install -y tzdata
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y git ninja-build cmake build-essential libopenblas-dev \
    xterm xauth openssh-server tmux wget mate-desktop-environment-core

RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

RUN pip install --upgrade pip
RUN pip install --ignore-installed blinker
RUN pip install spconv-cu118 easydict open3d

ENV CUDA_HOME=/usr/local/cuda-11.8


ENV TORCH_NVCC_FLAGS="-Xfatbin -compress-all"
ENV MAX_JOBS=4
# RUN apt-get install -y git ninja-build cmake build-essential libopenblas-dev \
#     xterm xauth openssh-server tmux wget mate-desktop-environment-core
RUN git clone --recursive "https://github.com/NVIDIA/MinkowskiEngine"
RUN cd MinkowskiEngine; python setup.py install --force_cuda --blas=openblas

WORKDIR /home/user/PCcompression
RUN unset DEBIAN_FRONTEND
RUN unset MAX_JOBS

RUN useradd -m user
RUN echo 'user ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
USER user
ENV USER=user
WORKDIR /home/user/
